package org.cubepuzzle;

import org.cubepuzzle.om.Puzzle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.cubepuzzle.util.MatrixUtils.mirrorMatrix;
import static org.cubepuzzle.util.MatrixUtils.rotateMatrix;

/**
 * Service class which tries to solve cube puzzle of 6 sides and return the list of puzzles in unfolded way. See method solve()
 */
public class PuzzleSolver {
    public static final int NUMBER_OF_POSSIBLE_ROTATES = 4;
    private List<Puzzle> puzzles;
    private int nextFirst = -1;

    public PuzzleSolver(List<Puzzle> puzzles) {
        this.puzzles = puzzles;
    }

    /**
     * solves the cube puzzle of 6 sides
     *
     * @return List of puzzles in unfolded way in the following order:<br/>
     * 1 2 3 <br/>
     * &nbsp;&nbsp;4<br/>
     * &nbsp;&nbsp;5<br/>
     * &nbsp;&nbsp;6<br/>
     * otherwise 'null' if puzzle cannot be solved
     */
    public List<Puzzle> solve() {
        List<Puzzle> result = null;

        for (int i = 0; i < puzzles.size(); i++) {
            nextFirst++;
            for (int j = 0; j <= NUMBER_OF_POSSIBLE_ROTATES * 2; j++) {
                result = trySolve();

                if (!isDone(result)) {
                    if (j == NUMBER_OF_POSSIBLE_ROTATES)
                        mirrorMatrix(puzzles.get(nextFirst).getMatrix());
                    else
                        rotateMatrix(puzzles.get(nextFirst).getMatrix());

                } else
                    return result;
            }
        }

        return result;
    }

    /**
     * Tries to solve current puzzles based on the list of availablePuzzles which has all original puzzles excluding first one.
     * Puzzle will be rotated 8 times including its mirror view while matching is happening.
     *
     * @return return List of puzzles which can be fit to each other.
     */
    private List<Puzzle> trySolve() {
        List<Puzzle> availablePuzzles = newAvailableList();
        List<Puzzle> pluggedPuzzles = newPluggedList();
        int i = 0;

        while (i < availablePuzzles.size()) {
            boolean noMatch = false;
            boolean mirrored = false;

            int rotated = 0;
            Puzzle anotherPuzzle = availablePuzzles.get(i);

            do {
                if (noMatch) {
                    if (rotated < NUMBER_OF_POSSIBLE_ROTATES) {
                        rotateMatrix(anotherPuzzle.getMatrix());
                        rotated++;
                    } else {
                        mirrorMatrix(anotherPuzzle.getMatrix());
                        mirrored = true;
                        rotated = 0;
                    }
                }
                noMatch = edgesNotMatch(anotherPuzzle, pluggedPuzzles);
            } while ((rotated < NUMBER_OF_POSSIBLE_ROTATES || !mirrored) && noMatch);

            if (noMatch) {
                i++;
            } else {
                pluggedPuzzles.add(anotherPuzzle);
                availablePuzzles.remove(i);
                i = 0;
            }
        }

        return pluggedPuzzles;
    }

    /**
     * @param anotherPuzzle  puzzle candidate to be fitted into a current cube state
     * @param pluggedPuzzles already plugged puzzles to be used 'as is' to fit the another puzzle
     * @return returns <b>true</b> if anotherPuzzle does not match already plugged puzzles. Each matching case depends on number of already
     * plugged puzzles and involves different number of edges and corners to compare whether they fit to each other at particular cube state <br/><br/>
     * <p>
     * return <b>false</b> if anotherPuzzle can be fit into current cube state based on pluggedPuzzles list
     */
    private boolean edgesNotMatch(Puzzle anotherPuzzle, List<Puzzle> pluggedPuzzles) {
        switch (pluggedPuzzles.size()) {
            case 1:
                Puzzle puzzleA = pluggedPuzzles.get(0);
                return notMatchedOneCornerOnly(puzzleA.getRightEdge(), anotherPuzzle.getLeftEdge());
            case 2:
                Puzzle puzzleB = pluggedPuzzles.get(1);
                return notMatchedOneCornerOnly(puzzleB.getRightEdge(), anotherPuzzle.getLeftEdge());
            case 3:
                puzzleA = pluggedPuzzles.get(0);
                puzzleB = pluggedPuzzles.get(1);
                Puzzle puzzleC = pluggedPuzzles.get(2);

                return notMatchedOneCornerOnly(puzzleB.getBottomEdge(), anotherPuzzle.getTopEdge()) ||
                        notMatchedOneCornerOnly(puzzleA.getBottomEdge(), anotherPuzzle.getLeftEdgeReflected()) ||
                        notMatchedOneCornerOnly(puzzleC.getBottomEdge(), anotherPuzzle.getRightEdge()) ||
                        !solidCorner(puzzleA.getBRCorner(), puzzleB.getBLCorner(), anotherPuzzle.getLTCorner()) ||
                        !solidCorner(puzzleB.getBRCorner(), puzzleC.getBLCorner(), anotherPuzzle.getRTCorner());
            case 4:
                puzzleA = pluggedPuzzles.get(0);
                puzzleC = pluggedPuzzles.get(2);
                Puzzle puzzleD = pluggedPuzzles.get(3);

                return notMatchedOneCornerOnly(puzzleD.getBottomEdge(), anotherPuzzle.getTopEdge()) ||
                        notMatchedOneCornerOnly(puzzleA.getLeftEdge(), anotherPuzzle.getLeftEdgeReflected()) ||
                        notMatchedOneCornerOnly(puzzleC.getRightEdge(), anotherPuzzle.getRightEdgeReflected()) ||
                        !solidCorner(puzzleD.getBRCorner(), puzzleC.getBRCorner(), anotherPuzzle.getRTCorner()) ||
                        !solidCorner(puzzleD.getBLCorner(), puzzleA.getBLCorner(), anotherPuzzle.getLTCorner());
            case 5:
                puzzleA = pluggedPuzzles.get(0);
                puzzleB = pluggedPuzzles.get(1);
                puzzleC = pluggedPuzzles.get(2);
                Puzzle puzzleE = pluggedPuzzles.get(4);

                return notMatchedOneCornerOnly(puzzleB.getTopEdge(), anotherPuzzle.getBottomEdge()) ||
                        notMatchedOneCornerOnly(puzzleA.getTopEdge(), anotherPuzzle.getLeftEdge()) ||
                        notMatchedOneCornerOnly(puzzleC.getTopEdge(), anotherPuzzle.getRightEdgeReflected()) ||
                        !solidCorner(puzzleA.getRTCorner(), puzzleB.getLTCorner(), anotherPuzzle.getBLCorner()) ||
                        !solidCorner(puzzleC.getLTCorner(), puzzleB.getRTCorner(), anotherPuzzle.getBRCorner()) ||
                        !solidCorner(puzzleA.getLTCorner(), puzzleE.getBLCorner(), anotherPuzzle.getLTCorner()) ||
                        !solidCorner(puzzleC.getRTCorner(), puzzleE.getBRCorner(), anotherPuzzle.getRTCorner());
        }

        return false;
    }

    /**
     * checks whether three different corners of separate puzzles form the solid corner and do not block each other
     *
     * @param corner1 corner of puzzle 1
     * @param corner2 corner of puzzle 2
     * @param corner3 corner of puzzle 3
     * @return returns true if sum of corner representation is equal to 1, it means that 2 puzzles have empty corner and third puzzle can fill the physical space and not collide with other puzzles <br/><br/>
     * return false if two or three puzzles collide and cannot form the right physical corner
     */
    private boolean solidCorner(int corner1, int corner2, int corner3) {
        return corner1 + corner2 + corner3 == 1;
    }

    /**
     * checks whether middle space of puzzle's edge fits to other puzzle's edge AND both puzzles' edges form either 0 or 1 corner at top or bottom, but not 2 corners
     *
     * @param edge1 edge of left puzzle
     * @param edge2 edge of right puzzle
     * @return true if middles collide or there is two corners fits
     * false if middles fit to each other and either 0 or 1 corner is formed on matching
     */
    private boolean notMatchedOneCornerOnly(byte[] edge1, byte[] edge2) {
        return middleNotMatched(edge1, edge2) || !oneCornerFits(edge1, edge2);
    }

    /**
     * check if two edges fit to each other in the middle
     *
     * @param edge1 edge of left puzzle
     * @param edge2 edge of right puzzle
     * @return true if middles collide, false otherwise
     */
    private boolean middleNotMatched(byte[] edge1, byte[] edge2) {
        for (int i = 1; i < edge1.length - 1; i++) {
            if (edge1[i] + edge2[i] != 1)
                return true;
        }
        return false;
    }

    /**
     * @param edge1 edge of left puzzle
     * @param edge2 edge of right puzzle
     * @return true if 0 or 1 corners fits, false otherwise
     */
    private boolean oneCornerFits(byte[] edge1, byte[] edge2) {
        byte edge1Corner1 = edge1[0];
        byte edge1Corner2 = edge1[edge1.length - 1];
        byte edge2Corner1 = edge2[0];
        byte edge2Corner2 = edge2[edge2.length - 1];

        return !(edge1Corner1 == Puzzle.FILLED_SPACE && edge2Corner1 == Puzzle.FILLED_SPACE) &&
                !(edge1Corner2 == Puzzle.FILLED_SPACE && edge2Corner2 == Puzzle.FILLED_SPACE);
    }

    /**
     * @return returns new List with available puzzles excluding one of the original puzzle by index nextFirst
     */
    private List<Puzzle> newAvailableList() {
        List<Puzzle> availableList = new ArrayList<>(puzzles);
        availableList.remove(nextFirst);
        return availableList;
    }

    /**
     * @return returns new List including one of the puzzle from original puzzle list by index nextFirst
     */
    private List<Puzzle> newPluggedList() {
        return new ArrayList<>(Arrays.asList(puzzles.get(nextFirst)));
    }

    /**
     * @param result list of plugged puzzles
     * @return returns true if current puzzles are plugged to each other
     */
    private boolean isDone(List<Puzzle> result) {
        return result.size() == puzzles.size();
    }
}
