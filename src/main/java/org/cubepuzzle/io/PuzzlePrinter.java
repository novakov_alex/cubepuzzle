package org.cubepuzzle.io;

import org.cubepuzzle.om.Puzzle;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Prints list of puzzles in unfolded way to provided PrintWriter
 */
public class PuzzlePrinter {
    /**
     * number of possible puzzles for 3d cube
     */
    public static final int PUZZLE_COUNT = 6;

    private int puzzleSize;
    private PrintWriter printWriter;

    public PuzzlePrinter(int puzzleSize, PrintWriter printWriter) {
        Objects.requireNonNull(printWriter, "printWriter should not be null");
        this.puzzleSize = puzzleSize;
        this.printWriter = printWriter;
    }

    /**
     * prints puzzles into printWriter if puzzles count == PUZZLE_COUNT
     *
     * @param puzzles list to be printed in unfolded way
     * @param message will be printed if puzzles count is not equal PUZZLE_COUNT
     */
    public void printUnfoldedOrElse(List<Puzzle> puzzles, String message) {
        try {
            if (puzzles == null || puzzles.size() != PUZZLE_COUNT) {
                printWriter.print(message);
            } else {
                List<byte[][]> line = createLine(puzzles.get(0), puzzles.get(1), puzzles.get(2));
                printMatrixInOneLine(line);

                line = createLine(null, puzzles.get(3), null);
                printMatrixInOneLine(line);

                line = createLine(null, puzzles.get(4), null);
                printMatrixInOneLine(line);

                line = createLine(null, puzzles.get(5), null);
                printMatrixInOneLine(line);
            }
        } finally {
            printWriter.flush();
        }
    }

    private List<byte[][]> createLine(Puzzle elem1, Puzzle elem2, Puzzle elem3) {
        List<byte[][]> line = new ArrayList<>(3);

        line.add(elem1 != null ? elem1.getMatrix() : null);
        line.add(elem2 != null ? elem2.getMatrix() : null);
        line.add(elem3 != null ? elem3.getMatrix() : null);

        return line;
    }

    private void printMatrixInOneLine(List<byte[][]> matrices) {
        for (int i = 0; i < puzzleSize; i++) {
            for (byte[][] matrix : matrices) {
                for (int j = 0; j < puzzleSize; j++) {
                    if (matrix == null)
                        printWriter.print(Puzzle.EMPTY_CODE);
                    else
                        printWriter.print(matrix[i][j] != Puzzle.FILLED_SPACE ? Puzzle.EMPTY_CODE : Puzzle.NONE_EMPTY_CODE);
                }
                printWriter.print(Puzzle.EMPTY_CODE);
            }
            printWriter.println();
        }
    }
}
