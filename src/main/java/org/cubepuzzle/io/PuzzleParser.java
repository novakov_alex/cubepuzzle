package org.cubepuzzle.io;

import org.cubepuzzle.om.Puzzle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Parse puzzles from ASCII presentation to list of Puzzle objects
 */
public class PuzzleParser {
    public static final String NUMBER_OF_CHARS_REGEXP = "(?<=\\G.{%d})";
    public static final String LINE_SEPARATOR = "\n";
    public static final int CELL_SIZE = 2;

    private int puzzleSize;
    private int numberOfPuzzles;
    private String puzzleSchema;

    public PuzzleParser(int puzzleSize, int numberOfPuzzles, String puzzleSchema) {
        this.puzzleSize = puzzleSize;
        this.numberOfPuzzles = numberOfPuzzles;
        this.puzzleSchema = puzzleSchema;
    }

    /**
     * Parse the provide puzzleSchema string
     *
     * @return list of parsed puzzles
     */
    public List<Puzzle> parse() {
        List<Puzzle> puzzles = new ArrayList<>();

        byte i = 1;
        List<String> lines = Arrays.asList(puzzleSchema.split(LINE_SEPARATOR));
        for (int page = 0; page < puzzleSize * numberOfPuzzles; page += puzzleSize) {
            puzzles.add(new Puzzle(parseRow(lines.stream().skip(page).limit(puzzleSize).collect(Collectors.toList())), i));
            i++;
        }

        return puzzles;
    }

    private byte[][] parseRow(List<String> lines) {
        byte[][] result = new byte[puzzleSize][puzzleSize];

        for (int i = 0; i < lines.size(); i++) {
            String[] cells = lines.get(i).split(String.format(NUMBER_OF_CHARS_REGEXP, CELL_SIZE));

            for (int j = 0; j < cells.length; j++) {
                if (cells[j].equals(Puzzle.NONE_EMPTY_CODE)) {
                    result[i][j] = Puzzle.FILLED_SPACE;
                }
            }
        }

        return result;
    }
}
