package org.cubepuzzle.om;

import org.cubepuzzle.util.MatrixUtils;

import java.util.Arrays;

/**
 * Model of Cube Puzzle based on 2-dimensional array
 */
public class Puzzle {
    public static final String NONE_EMPTY_CODE = "[]";
    public static final String EMPTY_CODE = "  ";
    public static final int FILLED_SPACE = 1;

    private byte[][] matrix;
    private byte index;

    public Puzzle(byte[][] matrix, byte index) {
        this.matrix = matrix;
        this.index = index;
    }

    public byte[][] getMatrix() {
        return matrix;
    }

    /**
     * @return array which represents right edge of the puzzle
     */
    public byte[] getRightEdge() {
        byte[] result = new byte[matrix.length];
        for (byte i = 0; i < matrix.length; i++) {
            result[i] = matrix[i][matrix.length - 1];
        }
        return result;
    }

    /**
     * @return array which represents left edge of the puzzle
     */
    public byte[] getLeftEdge() {
        byte[] result = new byte[matrix.length];
        for (int i = 0; i < matrix.length; i++) {
            result[i] = matrix[i][0];
        }
        return result;
    }

    /**
     * @return array which represents bottom edge of the puzzle
     */
    public byte[] getBottomEdge() {
        byte[] result = new byte[matrix.length];
        System.arraycopy(matrix[matrix.length - 1], 0, result, 0, matrix.length);
        return result;
    }

    /**
     * @return array which represents top edge of the puzzle
     */
    public byte[] getTopEdge() {
        byte[] result = new byte[matrix.length];
        System.arraycopy(matrix[0], 0, result, 0, matrix.length);
        return result;
    }

    public byte[] getLeftEdgeReflected() {
        return reflect(getLeftEdge());
    }

    public byte[] getRightEdgeReflected() {
        return reflect(getRightEdge());
    }

    public byte getBRCorner() {
        return getBottomEdge()[matrix.length - 1];
    }

    public int getBLCorner() {
        return getBottomEdge()[0];
    }

    public byte getLTCorner() {
        return getTopEdge()[0];
    }

    public byte getRTCorner() {
        return getTopEdge()[matrix.length - 1];
    }

    public byte getIndex() {
        return index;
    }

    private byte[] reflect(byte[] leftEdge) {
        return MatrixUtils.reverse(Arrays.copyOf(leftEdge, leftEdge.length));
    }
}
