package org.cubepuzzle.util;


public class MatrixUtils {

    /**
     * Rotates input 2-dimensional array by 90 degrees
     *
     * @param matrix input array to be rotated
     */
    public static void rotateMatrix(byte[][] matrix) {
        int n = matrix.length;
        for (int i = 0; i < n / 2; i++) {
            for (int j = 0; j < Math.ceil(((double) n) / 2.); j++) {
                byte temp = matrix[i][j];
                matrix[i][j] = matrix[n - 1 - j][i];
                matrix[n - 1 - j][i] = matrix[n - 1 - i][n - 1 - j];
                matrix[n - 1 - i][n - 1 - j] = matrix[j][n - 1 - i];
                matrix[j][n - 1 - i] = temp;
            }
        }
    }

    /**
     * reflects 2-dimensional array by right vertical axis
     *
     * @param matrix input array to be reflected
     */
    public static void mirrorMatrix(byte[][] matrix) {
        for (byte[] aM : matrix) {
            flipRow(aM);
        }
    }

    private static void flipRow(byte[] row) {
        int length = row.length;
        for (int i = 0; i < length / 2; i++) {
            byte x = row[i];
            row[i] = row[length - 1 - i];
            row[length - 1 - i] = x;
        }
    }

    /**
     * @param array input array to be reversed
     * @return reversed array
     */
    public static byte[] reverse(byte[] array) {
        for (int k = 0; k < array.length / 2; k++) {
            byte temp = array[k];
            array[k] = array[array.length - (1 + k)];
            array[array.length - (1 + k)] = temp;
        }

        return array;
    }
}
