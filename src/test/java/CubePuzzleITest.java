import org.cubepuzzle.PuzzleSolver;
import org.cubepuzzle.TestData;
import org.cubepuzzle.io.PuzzleParser;
import org.cubepuzzle.io.PuzzlePrinter;
import org.cubepuzzle.om.Puzzle;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.List;
import java.util.stream.IntStream;


@RunWith(BlockJUnit4ClassRunner.class)
public class CubePuzzleITest extends TestData {

    private static final String NO_SOLUTION_MESSAGE = "There is no solution";

    @Test
    public void bluePuzzleHasSolution() throws FileNotFoundException {
        //when
        List<Puzzle> result = getPuzzleSolver(bluePuzzle).solve();
        printToFile(result, "blue-puzzle.txt");
        //then
        Assert.assertTrue(NO_SOLUTION_MESSAGE, result != null && result.size() == 6);

        //when
        byte[] puzzleATopEdge = result.get(0).getTopEdge();
        byte[] puzzleELeftEdge = result.get(result.size() - 1).getLeftEdge();

        //then
        Assert.assertTrue(
                IntStream.range(0, puzzleATopEdge.length).map(i -> puzzleATopEdge[i]).sum() +
                        IntStream.range(0, puzzleELeftEdge.length).map(i -> puzzleELeftEdge[i]).sum()
                        <= 5
        );

        //when
        int indexC = result.get(2).getIndex();

        //then
        Assert.assertEquals("puzzleC should have index == 1", 1, indexC);
    }

    @Test
    public void redPuzzleHasSolution() throws FileNotFoundException {
        //when
        List<Puzzle> result = getPuzzleSolver(redPuzzle).solve();
        printToFile(result, "red-puzzle.txt");
        //then
        Assert.assertTrue(NO_SOLUTION_MESSAGE, result != null && result.size() == PuzzlePrinter.PUZZLE_COUNT);
    }

    @Test
    public void violetPuzzleHasSolution() throws FileNotFoundException {
        //when
        List<Puzzle> result = getPuzzleSolver(violetPuzzle).solve();
        printToFile(result, "violet-puzzle.txt");
        //then
        Assert.assertTrue(NO_SOLUTION_MESSAGE, result != null && result.size() == PuzzlePrinter.PUZZLE_COUNT);
    }

    @Test
    public void yellowPuzzleHasSolution() throws FileNotFoundException {
        //when
        List<Puzzle> result = getPuzzleSolver(yellowPuzzle).solve();
        printToFile(result, "yellow-puzzle.txt");
        //then
        Assert.assertTrue(NO_SOLUTION_MESSAGE, result != null && result.size() == PuzzlePrinter.PUZZLE_COUNT);
    }

    @Test
    public void myPuzzlePuzzleHasSolution() throws FileNotFoundException {
        //when
        List<Puzzle> result = getPuzzleSolver(myPuzzle).solve();
        printToFile(result, "my-puzzle.txt");
        //then
        Assert.assertTrue(NO_SOLUTION_MESSAGE, result != null && result.size() == PuzzlePrinter.PUZZLE_COUNT);
    }

    private PuzzleSolver getPuzzleSolver(String puzzleSchema) {
        PuzzleParser parser = new PuzzleParser(5, 6, puzzleSchema);
        List<Puzzle> parsedPuzzles = parser.parse();
        return new PuzzleSolver(parsedPuzzles);
    }

    private void printToFile(List<Puzzle> result, String fileName) throws FileNotFoundException {
        PrintStream printStream = new PrintStream(new File(fileName));
        PuzzlePrinter printer = new PuzzlePrinter(5, new PrintWriter(printStream));
        printer.printUnfoldedOrElse(result, NO_SOLUTION_MESSAGE);
    }
}
