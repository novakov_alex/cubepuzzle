package org.cubepuzzle.util;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;


@RunWith(BlockJUnit4ClassRunner.class)
public class MatrixUtilsTest {

    @Test
    public void testReverse() throws Exception {
        //given
        byte[] array = new byte[]{1, 0, 1, 0, 0};

        //when
        MatrixUtils.reverse(array);

        //then
        byte[] reversedArray = new byte[]{0, 0, 1, 0, 1};
        Assert.assertArrayEquals(reversedArray, array);
    }

    @Test
    public void rotateMatrix() {
        //given
        byte[][] matrixB = new byte[][]{
                {1, 0, 1, 0, 1},
                {1, 1, 1, 1, 1},
                {0, 1, 1, 1, 0},
                {1, 1, 1, 1, 1},
                {1, 0, 1, 0, 1}
        };

        //when
        MatrixUtils.rotateMatrix(matrixB);

        //then
        byte[][] puzzleBRotated90 = new byte[][]{
                {1, 1, 0, 1, 1},
                {0, 1, 1, 1, 0},
                {1, 1, 1, 1, 1},
                {0, 1, 1, 1, 0},
                {1, 1, 0, 1, 1}
        };
        Assert.assertArrayEquals(puzzleBRotated90, matrixB);
    }

    @Test
    public void mirrorMatrix() {
        //given
        byte[][] matrixC = new byte[][]{
                {0, 0, 1, 0, 0},
                {0, 1, 1, 1, 1},
                {1, 1, 1, 1, 0},
                {0, 1, 1, 1, 1},
                {0, 0, 1, 0, 0}
        };
        //when
        MatrixUtils.mirrorMatrix(matrixC);

        //then
        byte[][] puzzleCMirrored = new byte[][]{
                {0, 0, 1, 0, 0},
                {1, 1, 1, 1, 0},
                {0, 1, 1, 1, 1},
                {1, 1, 1, 1, 0},
                {0, 0, 1, 0, 0}
        };
        Assert.assertArrayEquals(puzzleCMirrored, matrixC);
    }
}