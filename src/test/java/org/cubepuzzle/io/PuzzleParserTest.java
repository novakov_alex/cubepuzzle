package org.cubepuzzle.io;

import org.cubepuzzle.TestData;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;


@RunWith(BlockJUnit4ClassRunner.class)
public class PuzzleParserTest extends TestData {

    @Test
    public void testParse() throws Exception {
        PuzzleParser parser = new PuzzleParser(5, 6, bluePuzzle);
        byte[][] bluePuzzle5 = new byte[][]{
                {0, 1, 0, 1, 0},
                {0, 1, 1, 1, 1},
                {1, 1, 1, 1, 0},
                {0, 1, 1, 1, 1},
                {1, 1, 0, 1, 1}
        };
        Assert.assertArrayEquals(bluePuzzle5, parser.parse().get(5).getMatrix());
    }
}